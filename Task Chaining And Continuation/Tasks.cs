﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TaskChainingAndContinuation;

namespace Task_Chaining_And_Continuation
{
    public static class Tasks
    {
        /// <summary>
        /// Converts GenerateIntArrayWithRandomValues method to task.
        /// </summary>
        /// <returns>Task</returns>
        public static Task<int[]> Task1()
        {
            var task = Task.Run(() => TaskMethods.GenerateIntArrayWithRandomValues());
            task.ContinueWith(
                antecedent =>
                {
                    Console.WriteLine("\nArray with random values");
                    PrintResult(antecedent.Result);
                }).Wait();

            return task;
        }

        /// <summary>
        /// Converts MultipyArrayByRandomInt method to task.
        /// </summary>
        /// <param name="array">Array to multiply</param>
        /// <returns>Task</returns>
        public static Task<int[]> Task2(int[] array)
        {
            var task = Task.Run(() => TaskMethods.MultipyArrayByRandomInt(array));
            task.ContinueWith(
                antecedent =>
                {
                    Console.WriteLine("\nArray Multiplied by random int");
                    PrintResult(antecedent.Result);
                }).Wait();

            return task;
        }

        /// <summary>
        /// Converts SortArrayAscending method to task.
        /// </summary>
        /// <param name="array">Array to sort.</param>
        /// <returns>Task</returns>
        public static Task<int[]> Task3(int[] array)
        {
            var task = Task.Run(() => TaskMethods.SortArrayAscending(array));
            task.ContinueWith(
                antecedent =>
                {
                    Console.WriteLine("\nArray sorted ascending");
                    PrintResult(antecedent.Result);
                }).Wait();

            return task;
        }

        /// <summary>
        /// Converts IntArrayAverage method to task.
        /// </summary>
        /// <param name="array">Array to find average value.</param>
        /// <returns>Task</returns>
        public static Task<double> Task4(int[] array)
        {
            var task = Task.Run(() => TaskMethods.IntArrayAverage(array));
            task.ContinueWith(
                antecedent =>
                {
                    Console.WriteLine("\nAverage");
                    Console.WriteLine(antecedent.Result);
                }).Wait();

            return task;
        }

        internal static void PrintResult(int[] array)
        {
            foreach (var item in array)
            {
                Console.Write($"{item} ");
            }
            Console.WriteLine();
        }
    }
}
