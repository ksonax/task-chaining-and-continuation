using NUnit.Framework;
using System;

namespace TaskChainingAndContinuation.Tests
{
    public sealed class Tests
    {

        private static readonly int[][] ToBigArrayData =
        {
            new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11 },
        };

        private static readonly int[][] CorrectArray =
        {
            new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 },
        };

        private static readonly int[][] SortArrayData =
{
            new int[] { 10, 9, 8, 7, 6, 5, 4, 3, 2, 1 },
        };

        /// <summary>
        /// Test if GenerateIntArrayWithRandomValues doesn't create empty array.
        /// </summary>
        [Test]
        public void GenerateIntArrayWithRandomValuesIsNotEmpty()
        {
            Assert.IsTrue(TaskMethods.GenerateIntArrayWithRandomValues().Length != 0);
        }

        /// <summary>
        /// Test if GenerateIntArrayWithRandomValues creates array with proper length.
        /// </summary>
        [Test]
        public void GenerateIntArrayWithRandomValueHas10Element()
        {
            Assert.IsTrue(TaskMethods.GenerateIntArrayWithRandomValues().Length == 10);
        }

        /// <summary>
        /// Test if MultipyArrayByRandomInt throws ArgumentNullException if array is null.
        /// </summary>
        /// <param name="array">Array to multiply.</param>
        [TestCase(null)]
        public void MultipyArrayByRandomIntThrowsArgumentNullExceptionWhenPassingNull(int[] array)
        {
            Assert.Throws<ArgumentNullException>(() => TaskMethods.MultipyArrayByRandomInt(array));
        }

        /// <summary>
        /// Test if MultipyArrayByRandomInt throws ArgumentException if array is too big.
        /// </summary>
        /// <param name="array">Array to multiply.</param>
        [TestCaseSource(nameof(ToBigArrayData))]
        public void MultipyArrayByRandomIntThrowsArgumentExceptionToBigArray(int[] array)
        {
            Assert.Throws<ArgumentException>(() => TaskMethods.MultipyArrayByRandomInt(array));
        }

        /// <summary>
        /// Test if MultipyArrayByRandomInt multiplies the array..
        /// </summary>
        /// <param name="array">Array to multiply.</param>
        [TestCaseSource(nameof(CorrectArray))]
        public void MultipyArrayByRandomIntReturnsCorrectlyMultipliedArray(int[] array)
        {
            int[] originalArray = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
            Assert.AreNotEqual(originalArray, TaskMethods.MultipyArrayByRandomInt(array));
        }

        /// <summary>
        /// Test if SortArrayAscending throws ArgumentNullException if array is null.
        /// </summary>
        /// <param name="array">Array to sort.</param>
        [TestCase(null)]
        public void SortArrayAscendingThrowsArgumentNullExceptionWhenPassingNull(int[] array)
        {
            Assert.Throws<ArgumentNullException>(() => TaskMethods.SortArrayAscending(array));
        }

        /// <summary>
        /// Test if SortArrayAscending throws ArgumentException if array is too big.
        /// </summary>
        /// <param name="array">Array to sort.</param>
        [TestCaseSource(nameof(ToBigArrayData))]
        public void SortArrayAscendingThrowsArgumentExceptionToBigArray(int[] array)
        {
            Assert.Throws<ArgumentException>(() => TaskMethods.SortArrayAscending(array));
        }

        /// <summary>
        /// Test if SortArrayAscending properly sorts the array.
        /// </summary>
        /// <param name="array">Array to sort.</param>
        [TestCaseSource(nameof(SortArrayData))]
        public void SortArrayAscendingCorectlySortsArray(int[] notSortedArray)
        {
            int[] sortedArray = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
            Assert.AreEqual(sortedArray, TaskMethods.SortArrayAscending(notSortedArray));
        }

        /// <summary>
        /// Test if IntArrayAverage ArgumentNullException if array is null.
        /// </summary>
        /// <param name="array">Array to calculate average value.</param>
        [TestCase(null)]
        public void IntArrayAverageThrowsArgumentNullExceptionWhenPassingNull(int[] array)
        {
            Assert.Throws<ArgumentNullException>(() => TaskMethods.IntArrayAverage(array));
        }

        /// <summary>
        /// Test if IntArrayAverage ArgumentException if array is too big.
        /// </summary>
        /// <param name="array">Array to calculate average value.</param>
        [TestCaseSource(nameof(ToBigArrayData))]
        public void IntArrayAverageThrowsArgumentExceptionToBigArray(int[] array)
        {
            Assert.Throws<ArgumentException>(() => TaskMethods.IntArrayAverage(array));
        }

        /// <summary>
        /// Test if IntArrayAverage calculates average properly.
        /// </summary>
        /// <param name="array">Array to calculate average value.</param>
        [TestCaseSource(nameof(CorrectArray))]
        public void IntArrayAverageReturnsCorrectAverage(int[] array)
        {
            Assert.AreEqual(5.5, TaskMethods.IntArrayAverage(array));
        }
    }
}