﻿using System;
using System.Linq;
using System.Text;

namespace TaskChainingAndContinuation
{
    public static class TaskMethods
    {
        private const int ArraySize = 10;
        private readonly static Random random = new Random();

        /// <summary>
        /// Generates array of random integers.
        /// </summary>
        /// <returns>Array of random integers.</returns>
        public static int[] GenerateIntArrayWithRandomValues()
        {
            var result = new int[ArraySize];
            for (var i = 0; i < ArraySize; i++)
            {
                result[i] = random.Next(0, 5);
            }

            return result;
        }

        /// <summary>
        /// Multiplies array by random value.
        /// </summary>
        /// <param name="array">Array to multiply.</param>
        /// <returns>Multipied int array.</returns>
        public static int[] MultipyArrayByRandomInt(int[] array)
        {
            CheckParameters(array);

            int randomInt = random.Next(1, 50);

            for (var i = 0; i < array.Length; i++)
            {
                array[i] *= randomInt;
            }

            return array;
        }

        /// <summary>
        /// Sorts array.
        /// </summary>
        /// <param name="array">Array to sort.</param>
        /// <returns>Sorted int array.</returns>
        public static int[] SortArrayAscending(int[] array)
        {
            CheckParameters(array);

            return array.OrderBy(x => x).ToArray();
        }

        /// <summary>
        /// Calculates average value of int array.
        /// </summary>
        /// <param name="array">Array to calculate average.</param>
        /// <returns>Average value of the array.</returns>
        public static double IntArrayAverage(int[] array)
        {
            CheckParameters(array);

            return array.Average(x => x);
        }

        internal static void CheckParameters(int[] array)
        {
            if (array == null)
            {
                throw new ArgumentNullException(nameof(array), "Array cannot be null!");
            }

            if (array.Length != ArraySize)
            {
                throw new ArgumentException($"Array should have {ArraySize} elements!");
            }
        }
    }
}
