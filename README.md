# Task Chaining And Continuation

Solution for Task Chaining And Continuation

# Tasks Description: 

Task 1: creates an array of 10 random integers.

Task 2: multiplies the array by a randomly generated number.

Task 3: sorts the array by ascending.

Task 4: calculates the average value.

# Program meets following criteria:

The tasks are built according to the description and execute the required functions.

The tasks are chained with the ContinueWith() method and the whole chain is awaited at once.

The program returns an average value of the antecedent array of integers.

