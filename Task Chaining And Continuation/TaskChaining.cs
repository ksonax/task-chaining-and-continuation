﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using TaskChainingAndContinuation;

namespace Task_Chaining_And_Continuation
{
    public static class TaskChaining
    {
        /// <summary>
        /// Chains all the <see cref="Tasks"/> methods and executes them.
        /// </summary>
        public static async Task Main()
        {
            var task = Tasks.Task1();
            await task.ContinueWith(
                antecedent =>
                {
                    Tasks.Task2(antecedent.Result)
                    .ContinueWith(
                        antecedent2 =>
                        {
                            Tasks.Task3(antecedent2.Result);
                            Tasks.Task4(antecedent2.Result);
                        }).Wait();
                });
        }
    }
}
